BEGIN {
    FS="-|,";
    part1_count = 0;
    part2_count = 0;
}

{
    part1_count += $1 >= $3 && $2 <= $4 || $3 >= $1 && $4 <= $2;
    part2_count += $1 <= $4 && $2 >= $3;
}

END {
    print "Day 04, part 1: " part1_count;
    print "Day 04, part 2: " part2_count;
}
