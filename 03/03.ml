(* Read lines from a file
   copied from: https://stackoverflow.com/a/23456034 *)
let read_lines name : string list =
  let ic = open_in name in
  let try_read () = try Some (input_line ic) with End_of_file -> None in
  let rec loop acc =
    match try_read () with
    | Some s -> loop (s :: acc)
    | None ->
        close_in ic;
        List.rev acc
  in
  loop []

let priority item =
  let code = Char.code item in
  if item < 'a' then code - 65 + 27 else code - 97 + 1

let rec sum = function
  | [] -> 0
  | head :: tail -> head + sum tail

let part1 string =
  let half_length = String.length string / 2 in
  let first_half = String.sub string 0 half_length in
  let last_half = String.sub string half_length half_length in
  let penalty looking_for =
    Bool.to_int (String.contains last_half looking_for) * priority looking_for
  in
    String.to_seq first_half |> List.of_seq |> List.sort_uniq compare |>
    List.map penalty |> sum

let rec into_three = function
  | [] -> [ [] ]
  | [ item1 ] -> [ [ item1 ] ]
  | [ item1; item2 ] -> [ [ item1; item2 ] ]
  | [ item1; item2; item3 ] -> [ [ item1; item2; item3 ] ]
  | item1 :: item2 :: item3 :: tail ->
      [ [ item1; item2; item3 ] ] @ into_three tail

let common_char lst =
  let arg1 = List.nth lst 0 in
  let arg2 = List.nth lst 1 in
  let arg3 = List.nth lst 2 in
  let rec _common_char s0 s1 s2 =
    let c = String.get s0 0 in
    let tail = String.sub s0 1 (String.length s0 - 1) in
    if String.contains s1 c && String.contains s2 c then c
    else _common_char tail s1 s2
  in
  _common_char arg1 arg2 arg3


let lines = read_lines "input";;

List.map part1 lines |> sum |> Format.printf "Day 03, part 1: %i\n";;

into_three lines |> List.map common_char |> List.map priority |> sum
|> Format.printf "Day 03, part 2: %i\n"
