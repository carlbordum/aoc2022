       IDENTIFICATION DIVISION.
           PROGRAM-ID.    AOC2022-DAY1.

       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT INPUT-FILE ASSIGN TO "input"
       	       ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION.
       FILE SECTION.
       FD INPUT-FILE.
           01 CALORIES      PIC 9(20).
       WORKING-STORAGE SECTION.
           01 END-OF-FILE   PIC Z(1).
           01 BEST          PIC 9(20).
           01 SECOND-BEST   PIC 9(20).
           01 THIRD-BEST    PIC 9(20).
           01 CURRENT-COUNT PIC 9(20).
           01 PART2-RESULT  PIC 9(20).

       PROCEDURE DIVISION.
           OPEN INPUT INPUT-FILE
           MOVE 0 TO END-OF-FILE.
           MOVE 0 TO BEST.
           MOVE 0 TO SECOND-BEST.
           MOVE 0 TO THIRD-BEST.
           MOVE 0 TO CURRENT-COUNT.
           MOVE 0 TO PART2-RESULT.
           
           PERFORM UNTIL END-OF-FILE = 1
               IF CALORIES = SPACE THEN
                   IF CURRENT-COUNT > BEST THEN
                       MOVE SECOND-BEST TO THIRD-BEST
                       MOVE BEST TO SECOND-BEST
                       MOVE CURRENT-COUNT TO BEST
                   ELSE
                       IF CURRENT-COUNT > SECOND-BEST THEN
                           MOVE SECOND-BEST TO THIRD-BEST
                           MOVE CURRENT-COUNT TO SECOND-BEST
                       ELSE
                           IF CURRENT-COUNT > THIRD-BEST THEN
                               MOVE CURRENT-COUNT TO THIRD-BEST
                           END-IF
                       END-IF
                   END-IF
                   MOVE 0 TO CURRENT-COUNT
               ELSE
                   ADD CALORIES TO CURRENT-COUNT
               END-IF

               READ INPUT-FILE
                   AT END MOVE 1 TO END-OF-FILE
               END-READ
           END-PERFORM

           CLOSE INPUT-FILE

           ADD THIRD-BEST SECOND-BEST TO BEST GIVING PART2-RESULT

           DISPLAY "Day 01, part 1:" SPACE BEST.
           DISPLAY "Day 01, part 2:" SPACE PART2-RESULT.
       STOP RUN.
