import sys

crates = []
commands = []

lines = iter(open("input").readlines())
for line in lines:
    if line == "\n":
        break
    crates.append(line)

for line in lines:
    commands.append(line)

with open("crates.txt", "w+") as f:
    for crate in zip(*reversed(crates)):
        if not crate[0].isspace():
            f.write("".join(crate).strip() + "\n")

with open("commands.vim", "w+") as f:
    f.write('gg"ayG')
    for cmd in commands:
        _, amount, _, src, _, dst = cmd.split()
        for _ in range(int(amount)):
            f.write(f":{src}\n$d$:{dst}\n$p")
    f.write(r":%s/.\+\(.\)/\1")
    f.write('\n:%j!\n"ap')
    for cmd in commands:
        _, amount, _, src, _, dst = cmd.split()
        src, dst = int(src) + 1, int(dst) + 1
        amount = f"{int(amount) - 1}h" if amount != "1" else ""
        f.write(f":{src}\n${amount}d$:{dst}\n$p")
    f.write(r":2,$s/.\+\(.\)/\1/g")
    f.write("\n:2,$j\n:2s/ //g\n")
    f.write(":1s/\\(.*\\)/Day 05, part 1: \\1\n")
    f.write(":2s/\\(.*\\)/Day 05, part 2: \\1\n")
