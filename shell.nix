with (import <nixpkgs> {});
mkShell {
  buildInputs = [
    gnu-cobol
    hare
    ocaml
    ocamlPackages.utop
  ];
}
